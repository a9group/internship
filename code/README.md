Ideal candidates will have experience configuring and managing a Linux
environment, including storage, databases, application and web services.

They will also possess and demonstrate some systems engineering skills suited to tasks.

You must be able to write a useful Linux shell script (we use bash,
please follow our lead)

You must also be able to read, interpret and create netowrking diagrams
in a variety of formats (and derive your own source of truth from them).
For example, translating the raw data presented in a Google Spreadsheet
to a Gliffy diagram.

Please use this directory to submit relevant samples of your code. For example:

* Items that demonѕtrate why we'd give you root access
* Shell utilities, services, clever one-liners (if it's public, provide
attribution, cite your source)
* An example .gliffy file that illustrates the following:

  ∗ An edge network
  * An edge firewall
  * A core network
  * A core firewall
  * A de-militarized zone
  * A private /24 network
  * A public /4 network
  * Shared storage available to both public and private networks (securely)

  Extra Credit:
  * Illustrate SPOFs
  * Show where to place a secure application server
  * Show where to place a web application server
  * Show where to place a storage application server
  * Where would you place a Directory Services server, and why?


Also, if you contribute to, or are involved in any public open source projects, we'd love to know about them.
This would be a good place to provide links with a description of your role in the project (so we don't have to sift through commit history to find your contributions).
